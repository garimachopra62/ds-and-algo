package arrayList;

import java.util.Scanner;
/*
 * The program is to demo the ArrayList functionality . The functionality is for mobile phone that contains Contact Information
 * 1) Adding Object in Array List
 * 2) Updating Array List
 * 3) Searching in ArrayList
 * 4) Delete in arrayList
 * 5) Fetching from ArrayList
 *  
 */
public class Main {
	
	private static MobilePhone mobilePhone = new MobilePhone();
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		getInstruction();
		boolean quit = false;
		while(!quit) {
		System.out.print("Print Choice\n");
		int choice = sc.nextInt();
		sc.nextLine();
		switch(choice) {
		case 1 : addContacts();break;
		case 2 : getContacts();break;
		case 3 : searchContacts();break;
		case 4 : modifyContacts();break;
		case 5 : deleteContacts();break;
		case 6 : getInstruction();break;
		case 7 : quit=true;System.out.print("Bye");break;
		}
		}
		
	}
	
	private static void getInstruction() {
		System.out.print("\nPlease choose among following listed options:\n"
				+ "1) To add Contacts\n"
				+ "2) To see Contact List\n"
				+ "3) To search Contact\n"
				+ "4) To modify Contact\n"
				+ "5) To delete Contact\n"
				+ "6) To get Instructions\n"
				+ "7) To quit\n"
				);
	}
	
	private static void addContacts() {
			System.out.print("\n*************ADD Contact**************\n");
			mobilePhone.addContacts(getInput());
		}

	private static void getContacts() {
			
			for(Contacts c : mobilePhone.getContactArrayList()) {
	            System.out.print("Name : " + c.getName() + ", Phone Number : " + c.getPhoneNo() + "\n");
	        }
	}
	
	private static void searchContacts() {
			
			int index  =  mobilePhone.getIndex(getInput());
			if(index >= 0 ) System.out.print("Contact Found\n");
			else System.out.print("Contact not in List\n");
	}
	
	private static void modifyContacts() {
				
			System.out.print("Please Enter Old Details\n");
	    	Contacts oldContact = getInput();
			System.out.print("Please Enter New Details\n");
			Contacts newContact = getInput();
			mobilePhone.updateContact(oldContact, newContact);
	}
		
	private static void deleteContacts() {
			
			mobilePhone.removeContact(getInput());
	}
		
	private static Contacts getInput() {
			System.out.print("Please enter Name\n");
			String name = sc.nextLine();
			System.out.print("please enter Phone number\n");
			String phoneNo = sc.nextLine();
			Contacts contact = new Contacts(name,phoneNo);
			return contact;
	}
}
