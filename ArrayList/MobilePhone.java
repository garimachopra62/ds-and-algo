package arrayList;

import java.util.ArrayList;
import java.util.List;

public class MobilePhone {

	private List<Contacts> contactArrayList = new ArrayList<>();
	
	public void addContacts(Contacts contact) {
		int index  = getIndex(contact);
		if(index < 0) contactArrayList.add(contact);
		else System.out.println("Contact Already Exist");
	}

	public List<Contacts> getContactArrayList() {
		return contactArrayList;
	}

	public void removeContact(Contacts contact) {
		int index  = getIndex(contact);
		if(index >= 0) contactArrayList.remove(index);
		else System.out.println("Contact Not Found");
		
	}
	
	public int getIndex(Contacts element) {
		for(int i=0 ; i < contactArrayList.size();i++) {
			if(contactArrayList.get(i).getName().equalsIgnoreCase(element.getName()) && contactArrayList.get(i).getPhoneNo().equalsIgnoreCase(element.getPhoneNo()) ) {
				return i;
			}
		}
		return -1;
		//	contactArrayList.contains(element); both these will work now as isequal method is implemented in contacts 
		// contactArrayList.indexOf(element);
	}
	
	
	public void updateContact(Contacts element,Contacts newElement) {
		int oldIndex = getIndex(element);
		int newIndex = getIndex(newElement);
		if(oldIndex >=0 && newIndex == -1 ) 
		contactArrayList.set(oldIndex, newElement);
		else 
		{  	if (oldIndex < 0)
			System.out.println("Contact Not found");
			else
			System.out.println("Contact Already exist found");
			
		}
	}
	
	
	
	
	
}
