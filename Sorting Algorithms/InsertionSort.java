/*
 * In Insertion sort , the sorted array is created from left to right 
 * Time complexity is O(n^2)
 * its a Stable Algorithm 
 */




public class InsertionSort {

	public static void main(String[] args) {
		int[] array = {20,35,-15,7,55,1,-22};
		for(int firstUnsortedIndex = 1 ; firstUnsortedIndex <= array.length-1 ; firstUnsortedIndex++) {
			int newElement = array[firstUnsortedIndex];
			int j;
			for( j=firstUnsortedIndex  ;j > 0 && array[j-1] > newElement  ;j--) {
					array[j] = array[j-1];
				
			}
			array[j] = newElement;
			
		}
		for(int c : array) {
			System.out.println(c);
		}

	}

}
/*
 * 

Loop Number :1
Final Array after loop 1
20,35,-15,7,55,1,-22,

Loop Number :2
First Sorted Index : 2 Value of J 2
First Sorted Index : 2 Value of J 1
Final Array after loop 2
-15,20,35,7,55,1,-22,

Loop Number :3
First Sorted Index : 3 Value of J 3
First Sorted Index : 3 Value of J 2
Final Array after loop 3
-15,7,20,35,55,1,-22,

Loop Number :4
Final Array after loop 4
-15,7,20,35,55,1,-22,

Loop Number :5
First Sorted Index : 5 Value of J 5
First Sorted Index : 5 Value of J 4
First Sorted Index : 5 Value of J 3
First Sorted Index : 5 Value of J 2
Final Array after loop 5
-15,1,7,20,35,55,-22,

Loop Number :6
First Sorted Index : 6 Value of J 6
First Sorted Index : 6 Value of J 5
First Sorted Index : 6 Value of J 4
First Sorted Index : 6 Value of J 3
First Sorted Index : 6 Value of J 2
First Sorted Index : 6 Value of J 1
Final Array after loop 6
-22,-15,1,7,20,35,55
 */
