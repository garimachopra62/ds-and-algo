
public class BubbleSort {
/* In bubble sort the last value of the array get sorted first thus creating a Partition array , 
 * In simple words the last index value get sorted first thus limiting the iteration to array.length - 1 for further loops 
 */
	
	public static void main(String[] args) {
		int[] array = {99,87,55,7,65,29,10};
		int temp =0;
		
		for(int i = array.length-1 ;i > 0 ;i--) {
			for (int j =0 ; j < i ; j++) {
				if(array[j] > array[j+1]) {
					temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
				}
			}
		}
		
		for (int i : array) {
			System.out.println(i);
		}
		

	}

}

/*
 * First Loop  
 *  
 * 1 87,99,55,7,65,29,100                                  
 * 2 87,55,99,7,65,29,100
 * 3 87,55,7,99,65,29,100
 * 4 87,55,7,65,99,29,100
 * 5 87,55,7,65,29,99,10
 * 6 87,55,7,65,29,10,99 
 *  
 * Second Loop   
 *  array = 87,55,7,65,29,10,99                                  
 * 1 55,87,7,65,29,10,99
 * 2 55,7,87,65,29,10,99
 * 3 55,7,65,87,29,10,99
 * 4 55,7,65,29,87,10,99
 * 5 55,7,65,29,10,87,99
 * 
 * Third loop 
 * 
 * array = 55,7,65,29,10,87,99
 * 1  7,55,65,29,10,87,99
 * 2  7,55,65,29,10,87,99
 * 3  8,55,29,65,10,87,99
 * 4  8,55,29,10,65,87,99
 * 
 * Fourth Loop
 * 
 * array = 8,55,29,10,65,87,99
 * 1  8,55,29,10,65,87,99
 * 2  8,29,55,10,65,87,99
 * 3  8,29,10,55,65,87,99
 * 
 * Fifth Loop 
 * array = 8,29,10,55,65,87,99
 * 1 8,29,10,55,65,87,99
 * 2 8,10,29,55,65,87,99
 * 
 * Sixth loop 
 * 8,10,29,55,65,87,99
 * 1 8,10,29,55,65,87,99
 * 
 * 
 * 
 *  */
