/* In selection Sort the Last Index gets the highest value first 
 * In this sort the highest value get directly swaped. 
 * Its not efficient bcoz of O(n^2)
 * Its unstable Sort 
 * 
 */
public class SelectionSort {

	public static void main(String[] args) {
		int[] myArray = {20,34,55,9,-1};
		for(int lastSortedIndex = myArray.length - 1 ; lastSortedIndex > 0 ; lastSortedIndex--){
			int largestIndex = 0;
			for(int i=1;i<=lastSortedIndex ;i++) {
				if(myArray[i] > myArray[largestIndex]) {
					largestIndex = i;
				}
				
		}
		
		int temp =0;
		temp = myArray[lastSortedIndex]	;
		myArray[lastSortedIndex] = myArray[largestIndex];
		myArray[largestIndex] = temp;
		
		}
		
		for(int c : myArray){
			System.out.println(c);
		}
		
		
		
	}

}

/*

Loop No 1
20,34,55,9,-1, Last Sorted Index 4 Largest Index 1
20,34,55,9,-1, Last Sorted Index 4 Largest Index 2
20,34,55,9,-1, Last Sorted Index 4 Largest Index 2
20,34,55,9,-1, Last Sorted Index 4 Largest Index 2
Final Array: 20,34,-1,9,55

Loop No 2
20,34,-1,9,55, Last Sorted Index 3 Largest Index 1
20,34,-1,9,55, Last Sorted Index 3 Largest Index 1
20,34,-1,9,55, Last Sorted Index 3 Largest Index 1
Final Array: 20,9,-1,34,55

Loop No 3
20,9,-1,34,55, Last Sorted Index 2 Largest Index 0
20,9,-1,34,55, Last Sorted Index 2 Largest Index 0
Final Array: -1,9,20,34,55

Loop No 4
-1,9,20,34,55, Last Sorted Index 1 Largest Index 1
Final Array: -1,9,20,34,55

*/
