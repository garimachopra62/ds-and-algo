import java.util.Scanner;


//The program is to find the Minimum value in an array 

public class Array {
	
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		int[] inputArray = getIntegers(); // Getting input from console
		
		printArray(inputArray); // Printing entered value
		
		System.out.println(minArray(inputArray)); // Logic to Find Minimum Value
		
		
		
	}
	
	public static int[] getIntegers() {
		System.out.println("How many numbers do you want to enter");
		int number = scan.nextInt();
		int[] myArray = new int[number];
		System.out.println("Enter Number");
		
		for(int i=0;i<number;i++) {
			myArray[i]= scan.nextInt();
		}
		return myArray;
	}
	
	public static void printArray(int[] values) {
		
		for(int i=0;i<values.length;i++) {
			System.out.println("Value of "+ i +" Element is " + values[i]);
		}
		
	}
	
	public static int minArray(int[] inputArray) {
		var minValue = inputArray[0]; 
		//{1,6,0,12}
		for(int i = 1 ; i < inputArray.length ;i++) {
			if(minValue > inputArray[i]) {
				minValue = inputArray[i];
			}
		}
		return minValue;
	}		
	
}
