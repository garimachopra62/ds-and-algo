import java.util.Arrays;
import java.util.Scanner;


//The program is to Reverse the Array

public class Array {
	
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		int[] inputArray = getIntegers(); // Getting input from console
		
		printArray(inputArray); // Printing entered value
		
		System.out.println(Arrays.toString(reverseArray(inputArray))); // Logic to reverse the Array 
		
		
	}
	
	public static int[] getIntegers() {
		System.out.println("How many numbers do you want to enter");
		int number = scan.nextInt();
		int[] myArray = new int[number];
		System.out.println("Enter Number");
		
		for(int i=0;i<number;i++) {
			myArray[i]= scan.nextInt();
		}
		return myArray;
	}
	
	public static void printArray(int[] values) {
		
		for(int i=0;i<values.length;i++) {
			System.out.println("Value of "+ i +" Element is " + values[i]);
		}
		
	}
	
	
	
	public static int[] reverseArray(int[] array) {
		int temp=0;
		for(int i=0;i<array.length/2;i++) {
			temp = array[i];
			array[i]= array[array.length - i-1];
			array[array.length -i-1 ] = temp;
		}
		return array;
		
	}
}
