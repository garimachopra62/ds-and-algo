import java.util.Scanner;
//The program is to sort the array in descending order using two ways , One is using two for loops and another one is using While loop
public class Array {
	
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		int[] inputArray = getIntegers(); // Getting input from console
		
		printArray(inputArray); // Printing entered value
		
		int[] sortedArray = sortArray(inputArray); // Logic to perform sort
		
		System.out.println("Sorted Values are");
		for(int i=0;i<sortedArray.length;i++) {   // Printing Sorted values
			System.out.println(sortedArray[i]);
		}
	}
	
	public static int[] getIntegers() {
		System.out.println("How many numbers do you want to enter");
		int number = scan.nextInt();
		int[] myArray = new int[number];
		System.out.println("Enter Number");
		
		for(int i=0;i<number;i++) {
			myArray[i]= scan.nextInt();
		}
		return myArray;
	}
	
	public static void printArray(int[] values) {
		
		for(int i=0;i<values.length;i++) {
			System.out.println("Value of "+ i +" Element is " + values[i]);
		}
		
	}
	
	public static int[] sortArray(int[] inputArray) {
		int temp=0;
		for(int i=0 ;i< inputArray.length ;i++) { 
			for(int j=i+1 ;j<inputArray.length;j++) {
				if(inputArray[j] > inputArray[i]) {
					temp = inputArray[i];
					inputArray[i] = inputArray[j];
					inputArray[j]= temp;
				}
			}
			
		}

		
//		 Another way using While loop 
//		boolean flag = true;
//		while(flag) {
//			flag = false;
//			
//			for(int i=0;i < inputArray.length-1;i++) {
//				if(inputArray[i] < inputArray[i+1]) {
//					temp = inputArray[i];
//					inputArray[i]= inputArray[i+1];
//					inputArray[i+1] = temp;
//					flag = true;
//				}
//			}
//		}
		return inputArray;
	}
	
}
